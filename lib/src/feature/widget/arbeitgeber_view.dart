import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tomisha/src/core/constant/string_constant.dart';
import 'package:tomisha/src/feature/widget/first_point.dart';
import 'package:tomisha/src/feature/widget/first_point_web.dart';
import 'package:tomisha/src/feature/widget/second_point.dart';
import 'package:tomisha/src/feature/widget/second_point_web.dart';
import 'package:tomisha/src/feature/widget/third_grey_circle.dart';
import 'package:tomisha/src/feature/widget/third_point.dart';
import 'package:tomisha/src/feature/widget/third_point_web.dart';
import 'package:tomisha/src/widget/adaptive/adaptive_base_widget.dart';
import 'package:tomisha/src/widget/responsive/responsive_extensions.dart';

class ArbeitgeberView extends StatefulWidget {
  const ArbeitgeberView({Key? key, required this.selectedIndex})
      : super(key: key);

  final int selectedIndex;

  @override
  State<ArbeitgeberView> createState() => _ArbeitgeberViewState();
}

class _ArbeitgeberViewState extends State<ArbeitgeberView> {
  final GlobalKey thirdNumberKey = GlobalKey();
  double? position = 0;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_afterLayout);
  }

  _afterLayout(_) {
    _getPosition();
  }

  _getPosition() async {
    final RenderBox rb =
        thirdNumberKey.currentContext?.findRenderObject() as RenderBox;
    position = rb.localToGlobal(Offset.zero).dx;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    _getPosition();

    return AdaptiveBuilder(
      builder: (context, sizingInformation) {
        return Stack(
          children: [
            if (sizingInformation.isWebMobile ||
                sizingInformation.isNativeMobile)
              const ThirdGreyCircle(),
            Column(
              children: [
                if (sizingInformation.isWebMobile ||
                    sizingInformation.isNativeMobile)
                  const FirstPoint(
                    title: 'Erstellen dein Unternehmensprofil',
                    imagePath: StringConstant.undrawProfileData,
                  )
                else ...[
                  const SizedBox(
                    height: 20,
                  ),
                  const FirstPointWeb(
                    title: 'Erstellen dein Unternehmensprofil',
                    imagePath: StringConstant.undrawProfileData,
                  ),
                ],
                if (sizingInformation.isWebMobile ||
                    sizingInformation.isNativeMobile)
                  SecondPoint(
                    title: 'Erstellen ein Jobinserat',
                    imagePath: StringConstant.undrawAboutMe,
                    index: widget.selectedIndex,
                  )
                else ...[
                  const SizedBox(
                    height: 20,
                  ),
                  SecondPointWeb(
                    title: 'Erstellen dein Lebenslauf',
                    imagePath: StringConstant.undrawTask,
                    index: widget.selectedIndex,
                  ),
                ],
                if (sizingInformation.isWebMobile ||
                    sizingInformation.isNativeMobile)
                  ThirdPoint(
                    imagePath: StringConstant.undrawSwipeProfile,
                    title: 'Wähle deinen neuen Mitarbeiter aus',
                    index: widget.selectedIndex,
                  )
                else ...[
                  const SizedBox(
                    height: 20,
                  ),
                  ThirdPointWeb(
                    thirdNumberKey: thirdNumberKey,
                    imagePath: StringConstant.undrawPersonalFile,
                    title: 'Wähle deinen neuen Mitarbeiter aus',
                    index: widget.selectedIndex,
                  ),
                ],
              ],
            ),
            if (sizingInformation.isDesktop || sizingInformation.isDesktopLarge)
              Positioned(
                top: 180.h,
                left: 360.w,
                child: SizedBox(
                  height: 150.h,
                  width: 406.w,
                  child: SvgPicture.asset(StringConstant.firstArrow,
                      fit: BoxFit.fill),
                ),
              ),
            if (sizingInformation.isDesktop || sizingInformation.isDesktopLarge)
              Positioned(
                top: 450.h,
                left: position,
                child: SizedBox(
                  height: 200.h,
                  width: 440.w,
                  child: SvgPicture.asset(StringConstant.secondArrow,
                      fit: BoxFit.fill),
                ),
              ),
          ],
        );
      },
    );
  }
}
