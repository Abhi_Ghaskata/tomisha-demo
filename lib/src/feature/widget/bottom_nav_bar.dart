import 'package:flutter/material.dart';
import 'package:tomisha/src/core/constant/color_constant.dart';
import 'package:tomisha/src/widget/app_elevated_button.dart';

class BottomNavBarApp extends StatelessWidget {
  const BottomNavBarApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.sizeOf(context).width;

    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(30),
        ),
        boxShadow: [
          BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 10),
        ],
      ),
      child: ClipRRect(
        borderRadius: const BorderRadius.vertical(
          top: Radius.circular(30),
        ),
        child: BottomAppBar(
          child: SizedBox(
            height: 80,
            child: Center(
              child: Container(
                width: screenWidth * 0.8,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  gradient: const LinearGradient(
                    colors: [
                      ColorConstant.green_100,
                      ColorConstant.green_300,
                    ],
                  ),
                ),
                child: const AppElevatedButton(),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
