import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tomisha/src/core/constant/color_constant.dart';
import 'package:tomisha/src/theme/app_text_theme.dart';
import 'package:tomisha/src/widget/responsive/responsive_extensions.dart';

class FirstPoint extends StatelessWidget {
  const FirstPoint({
    Key? key,
    required this.title,
    required this.imagePath,
  }) : super(key: key);

  final String title;
  final String imagePath;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned(
          bottom: 50,
          child: Container(
            height: 150.h,
            width: 150.w,
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              color: ColorConstant.greyCircle,
            ),
          ),
        ),
        Column(
          children: [
            SvgPicture.asset(
              imagePath,
              height: 145.h,
              width: 220.w,
            ),
            Transform.translate(
              offset: const Offset(0, -60),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Center(
                    child: Text(
                      '1.',
                      style: AppTextTheme(context).h1?.copyWith(
                            fontSize: 130,
                            color: ColorConstant.fontColor,
                          ),
                    ),
                  ),
                  SizedBox(width: 23.w),
                  Flexible(
                    child: Transform.translate(
                      offset: const Offset(0, -30),
                      child: Text(
                        title,
                        style: AppTextTheme(context).body?.copyWith(
                              color: ColorConstant.fontColor,
                            ),
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}
