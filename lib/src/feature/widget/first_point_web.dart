import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tomisha/src/core/constant/color_constant.dart';
import 'package:tomisha/src/theme/app_text_theme.dart';
import 'package:tomisha/src/widget/responsive/responsive_extensions.dart';

class FirstPointWeb extends StatelessWidget {
  const FirstPointWeb({
    Key? key,
    required this.title,
    required this.imagePath,
  }) : super(key: key);

  final String title;
  final String imagePath;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Spacer(),
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Container(
              height: 150.h,
              width: 150.w,
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: ColorConstant.greyCircle,
              ),
              child: Center(
                child: Text(
                  '1.',
                  style: AppTextTheme(context).h1?.copyWith(
                        fontSize: 130,
                        fontWeight: FontWeight.w700,
                        color: ColorConstant.fontColor,
                        height: 0,
                      ),
                ),
              ),
            ),
            Transform.translate(
              offset: const Offset(0, -20),
              child: Text(
                title,
                style: AppTextTheme(context).body?.copyWith(
                      color: ColorConstant.fontColor,
                    ),
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
        SizedBox(
          width: 61.w,
        ),
        SvgPicture.asset(
          imagePath,
          height: 145,
          width: 220,
        ),
        const Spacer(),
        const Spacer()
      ],
    );
  }
}
