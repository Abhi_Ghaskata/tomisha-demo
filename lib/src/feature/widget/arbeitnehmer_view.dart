import 'package:flutter/material.dart';
import 'package:tomisha/src/core/constant/string_constant.dart';
import 'package:tomisha/src/feature/widget/first_point.dart';
import 'package:tomisha/src/feature/widget/second_point.dart';
import 'package:tomisha/src/feature/widget/third_grey_circle.dart';
import 'package:tomisha/src/feature/widget/third_point.dart';
import 'package:tomisha/src/widget/adaptive/adaptive_base_widget.dart';

class ArbeitnehmerView extends StatelessWidget {
  const ArbeitnehmerView({Key? key, required this.selectedIndex})
      : super(key: key);

  final int selectedIndex;

  @override
  Widget build(BuildContext context) {
    return AdaptiveBuilder(
      builder: (context, sizingInformation) => Stack(
        children: [
          if (sizingInformation.isWebMobile || sizingInformation.isNativeMobile)
            const ThirdGreyCircle(),
          Column(
            children: [
              const FirstPoint(
                imagePath: StringConstant.undrawProfileData,
                title: 'Erstellen dein Lebenslauf',
              ),
              SecondPoint(
                title: 'Erstellen dein Lebenslauf',
                imagePath: StringConstant.undrawTask,
                index: selectedIndex,
              ),
              ThirdPoint(
                imagePath: StringConstant.undrawPersonalFile,
                title: 'Mit nur einem Klick\nbewerben',
                index: selectedIndex,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
