import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:proste_bezier_curve/proste_bezier_curve.dart';
import 'package:tomisha/src/core/constant/color_constant.dart';
import 'package:tomisha/src/theme/app_text_theme.dart';
import 'package:tomisha/src/widget/responsive/responsive_extensions.dart';

class SecondPointWeb extends StatelessWidget {
  const SecondPointWeb({
    Key? key,
    required this.title,
    required this.imagePath,
    required this.index,
  }) : super(key: key);

  final String title;
  final String imagePath;
  final int index;

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.sizeOf(context).width;

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ClipPath(
          clipper: ProsteBezierCurve(
            position: ClipPosition.top,
            list: [
              BezierCurveSection(
                start: Offset(screenWidth, 60),
                top: Offset(screenWidth / 4 * 3, 0),
                end: Offset(screenWidth / 2, 30),
              ),
              BezierCurveSection(
                start: Offset(screenWidth / 2, 0),
                top: Offset(screenWidth / 4, 30),
                end: const Offset(0, 0),
              ),
            ],
          ),
          child: ClipPath(
            clipper: ProsteBezierCurve(
              position: ClipPosition.bottom,
              list: [
                BezierCurveSection(
                  start: const Offset(0, 350),
                  top: Offset(screenWidth / 4, 320),
                  end: Offset(screenWidth / 2, 325),
                ),
                BezierCurveSection(
                  start: Offset(screenWidth / 2, 325),
                  top: Offset(screenWidth / 4 * 3, 350),
                  end: Offset(screenWidth, 250),
                ),
              ],
            ),
            child: Container(
              height: 370,
              width: screenWidth,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    ColorConstant.green_200,
                    ColorConstant.background,
                  ],
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    imagePath,
                    height: 180.h,
                    width: 342.w,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 37),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          '2.',
                          style: AppTextTheme(context).h1?.copyWith(
                                fontSize: 130,
                                color: ColorConstant.fontColor,
                              ),
                        ),
                        Transform.translate(
                          offset: const Offset(0, -30),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 23),
                            child: Text(
                              title,
                              style: AppTextTheme(context).body?.copyWith(
                                    color: ColorConstant.fontColor,
                                  ),
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
