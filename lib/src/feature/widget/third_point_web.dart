import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tomisha/src/core/constant/color_constant.dart';
import 'package:tomisha/src/theme/app_text_theme.dart';
import 'package:tomisha/src/widget/responsive/responsive_extensions.dart';

class ThirdPointWeb extends StatelessWidget {
  const ThirdPointWeb({
    Key? key,
    required this.title,
    required this.imagePath,
    required this.index,
    required this.thirdNumberKey,
  }) : super(key: key);

  final String title;
  final String imagePath;
  final int index;
  final Key thirdNumberKey;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Flexible(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Flexible(
                flex: 2,
                child: Stack(
                  children: [
                    Positioned.fill(
                      left: -240,
                      child: Container(
                        height: 304.h,
                        width: 304.w,
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: ColorConstant.greyCircle,
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          '3.',
                          style: AppTextTheme(context).h1?.copyWith(
                                fontSize: 156,
                                color: ColorConstant.fontColor,
                              ),
                        ),
                        Flexible(
                          key: thirdNumberKey,
                          flex: 2,
                          child: Transform.translate(
                            offset: const Offset(0, 50),
                            child: Padding(
                              padding: const EdgeInsets.only(left: 23),
                              child: Text(
                                title,
                                style: AppTextTheme(context).body?.copyWith(
                                      color: ColorConstant.fontColor,
                                    ),
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 27.w,
              ),
              Flexible(
                flex: 2,
                child: SvgPicture.asset(
                  imagePath,
                  height: 300.h,
                  width: 502.w,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
