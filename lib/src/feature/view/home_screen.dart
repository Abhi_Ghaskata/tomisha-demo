import 'package:flutter/material.dart';
import 'package:proste_bezier_curve/proste_bezier_curve.dart';
import 'package:tomisha/src/core/constant/color_constant.dart';
import 'package:tomisha/src/core/constant/string_constant.dart';
import 'package:tomisha/src/feature/widget/arbeitgeber_view.dart';
import 'package:tomisha/src/feature/widget/arbeitnehmer_view.dart';
import 'package:tomisha/src/feature/widget/bottom_nav_bar.dart';
import 'package:tomisha/src/feature/widget/temporar_office_view.dart';
import 'package:tomisha/src/theme/app_text_theme.dart';
import 'package:tomisha/src/widget/adaptive/adaptive_base_widget.dart';
import 'package:tomisha/src/widget/app_elevated_button.dart';
import 'package:tomisha/src/widget/app_text_button.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tomisha/src/widget/responsive/responsive_extensions.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  BorderRadius dynamicBorder = const BorderRadius.only(
    topLeft: Radius.circular(12),
    bottomLeft: Radius.circular(12),
  );
  int selectedIndex = 0;
  final List<bool> toggleButtonItems = [true, false, false];

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.sizeOf(context).width;

    String titleText = '';
    switch (selectedIndex) {
      case 0:
        titleText = 'Drei einfache Schritte\nzu deinem neuen Job';
        break;
      case 1:
        titleText = 'Drei einfache Schritte\nzu deinem neuen Mitarbeiter';
        break;
      case 2:
        titleText = 'Drei einfache Schritte zur\nVermittlung neuer Mitarbeiter';
        break;
      default:
    }

    return Scaffold(
      backgroundColor: ColorConstant.white,
      appBar: AppBar(
        toolbarHeight: 67.h,
        backgroundColor: ColorConstant.white,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            AppTextButton(
              onPressed: () {},
              text: 'Login',
              textColor: ColorConstant.green_100,
            ),
          ],
        ),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(12),
          ),
        ),
      ),
      body: AdaptiveBuilder(
        builder: (context, sizingInformation) {
          if (sizingInformation.isNativeMobile ||
              sizingInformation.isWebMobile) {
            return _mobileView(screenWidth, context, titleText);
          }
          return _webView(screenWidth, context, titleText);
        },
      ),
      bottomNavigationBar: AdaptiveBuilder(
          builder: (context, sizingInformation) =>
              (sizingInformation.isNativeMobile ||
                      sizingInformation.isWebMobile)
                  ? const BottomNavBarApp()
                  : const SizedBox.shrink()),
    );
  }

  SingleChildScrollView _mobileView(
      double screenWidth, BuildContext context, String titleText) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Transform.translate(
            offset: const Offset(0, -10),
            child: ClipPath(
              clipper: ProsteBezierCurve(
                position: ClipPosition.bottom,
                list: [
                  BezierCurveSection(
                    start: const Offset(0, 625),
                    top: Offset(screenWidth / 4, 640),
                    end: Offset(screenWidth / 2, 625),
                  ),
                  BezierCurveSection(
                    start: Offset(screenWidth / 2, 625),
                    top: Offset(screenWidth / 4 * 3, 600),
                    end: Offset(screenWidth, 600),
                  ),
                ],
              ),
              child: Container(
                height: 700,
                width: screenWidth,
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      ColorConstant.background,
                      ColorConstant.green_200,
                    ],
                    begin: Alignment.centerRight,
                    end: Alignment.bottomRight,
                    stops: [0, 0.3],
                  ),
                ),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 30,
                    ),
                    Text(
                      'Deine Job\nwebsite',
                      style: AppTextTheme(context).h1?.copyWith(fontSize: 50),
                      textAlign: TextAlign.center,
                    ),
                    SvgPicture.asset(
                      StringConstant.undrawAgreement,
                      fit: BoxFit.fill,
                      width: screenWidth,
                      height: 436,
                    ),
                  ],
                ),
              ),
            ),
          ),
          _toggleButton(titleText, isMobile: true),
          if (selectedIndex == 0)
            ArbeitnehmerView(selectedIndex: selectedIndex),
          if (selectedIndex == 1) ArbeitgeberView(selectedIndex: selectedIndex),
          if (selectedIndex == 2)
            TemporaryOfficeView(selectedIndex: selectedIndex),
        ],
      ),
    );
  }

  ListView _webView(
      double screenWidth, BuildContext context, String? titleText) {
    return ListView(
      children: [
        Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            Transform.translate(
              offset: const Offset(0, -10),
              child: ClipPath(
                clipper: ProsteBezierCurve(
                  position: ClipPosition.bottom,
                  list: [
                    BezierCurveSection(
                      start: const Offset(0, 425),
                      top: Offset(screenWidth / 4, 440),
                      end: Offset(screenWidth / 2, 425),
                    ),
                    BezierCurveSection(
                      start: Offset(screenWidth / 2, 425),
                      top: Offset(screenWidth / 4 * 3, 400),
                      end: Offset(screenWidth, 400),
                    ),
                  ],
                ),
                child: Container(
                  height: 450,
                  width: screenWidth,
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        ColorConstant.background,
                        ColorConstant.green_200,
                      ],
                    ),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Deine Job\nwebsite',
                            style: AppTextTheme(context).h1?.copyWith(
                                fontSize: 50, color: ColorConstant.black),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(height: 65.h),
                          Container(
                            width: screenWidth * 0.2,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              gradient: const LinearGradient(
                                colors: [
                                  ColorConstant.green_100,
                                  ColorConstant.green_300,
                                ],
                              ),
                            ),
                            child: const AppElevatedButton(),
                          ),
                        ],
                      ),
                      SizedBox(
                        width: 156.w,
                      ),
                      CircleAvatar(
                        backgroundColor: ColorConstant.white,
                        radius: 156,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(200),
                          child: SvgPicture.asset(
                            StringConstant.undrawAgreement,
                            height: 455.h,
                            width: 455.w,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
        _toggleButton(titleText),
        ArbeitgeberView(selectedIndex: selectedIndex),
        SizedBox(
          height: 200.h,
        ),
      ],
    );
  }

  Widget _toggleButton(String? titleText, {bool isMobile = false}) {
    return Column(
      children: [
        const SizedBox(
          height: 40,
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Scrollbar(
            child: Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: ToggleButtons(
                onPressed: (int index) {
                  setState(() {
                    // The button that is tapped is set to true, and the others to false.
                    for (int i = 0; i < toggleButtonItems.length; i++) {
                      toggleButtonItems[i] = i == index;
                      selectedIndex = index;
                    }
                  });
                },
                borderRadius: BorderRadius.circular(12),
                borderColor: ColorConstant.grey,
                selectedBorderColor: ColorConstant.toggleButtonSelected,
                selectedColor: Colors.white,
                textStyle: AppTextTheme(context).h3,
                fillColor: ColorConstant.toggleButtonSelected,
                color: ColorConstant.green_100,
                constraints: const BoxConstraints(
                  minHeight: 40.0,
                  minWidth: 80.0,
                ),
                isSelected: toggleButtonItems,
                children: const [
                  SizedBox(
                    width: 160,
                    child: Center(
                      child: Text('Arbeitnehmer'),
                    ),
                  ),
                  SizedBox(
                    width: 160,
                    child: Center(
                      child: Text('Arbeitgeber'),
                    ),
                  ),
                  SizedBox(
                    width: 160,
                    child: Center(
                      child: Text('Temporärbüro'),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        Text(
          titleText ?? '',
          textAlign: TextAlign.center,
          style: AppTextTheme(context).h2?.copyWith(
                color: ColorConstant.titleColor,
                fontSize: isMobile ? 21 : 40,
              ),
        ),
        const SizedBox(
          height: 20,
        ),
      ],
    );
  }
}
