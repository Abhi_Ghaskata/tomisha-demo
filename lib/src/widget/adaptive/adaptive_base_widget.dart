import 'package:flutter/material.dart';
import 'package:tomisha/src/widget/adaptive/adaptive_breakpoints.dart';
import 'package:tomisha/src/widget/adaptive/adaptive_sizing.dart';

class AdaptiveBuilder extends StatelessWidget {
  const AdaptiveBuilder({Key? key, required this.builder}) : super(key: key);

  final Widget Function(
      BuildContext context, AdaptiveSizingInfo sizingInformation) builder;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, boxConstraints) {
        final mediaQuery = MediaQuery(
          data: MediaQueryData.fromView(View.of(context)),
          child: LayoutBuilder(builder: (_, BoxConstraints constraints) {
            return const SizedBox.shrink();
          }),
        );
        final sizingInformation = AdaptiveSizingInfo(
          deviceType: getDeviceType(mediaQuery.data),
          screenSize: mediaQuery.data.size,
          localWidgetSize:
              Size(boxConstraints.maxWidth, boxConstraints.maxHeight),
        );
        return builder(context, sizingInformation);
      },
    );
  }
}
