import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:tomisha/src/widget/adaptive/adaptive_screen_type.dart';

AdaptiveScreenType getDeviceType(MediaQueryData mediaQuery) {
  double deviceWidth = mediaQuery.size.width;

  return kIsWeb
      ? deviceWidth > 1024
          ? AdaptiveScreenType.desktop
          : deviceWidth > 678
              ? AdaptiveScreenType.webTablet
              : AdaptiveScreenType.webMobile
      : deviceWidth > 678
          ? AdaptiveScreenType.nativeTablet
          : AdaptiveScreenType.nativeMobile;
}
