import 'package:flutter/material.dart';
import 'package:tomisha/src/core/constant/color_constant.dart';
import 'package:tomisha/src/theme/app_text_theme.dart';

class AppElevatedButton extends StatelessWidget {
  const AppElevatedButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        backgroundColor: ColorConstant.transparent,
        surfaceTintColor: ColorConstant.transparent,
        shadowColor: ColorConstant.transparent,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
        minimumSize: const Size(
          double.infinity,
          50,
        ),
      ),
      onPressed: () {},
      child: Text(
        'Kostenlos Registrieren',
        style: AppTextTheme(context)
            .bodySmall
            ?.copyWith(color: ColorConstant.white),
      ),
    );
  }
}
