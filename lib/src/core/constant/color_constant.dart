import 'package:flutter/material.dart';

class ColorConstant {
  ColorConstant._();

  static const white = Colors.white;
  static const green_100 = Color(0xff319795);
  static const green_200 = Color(0xffE6FFFA);
  static const green_300 = Color(0xff3182CE);
  static const background = Color(0xffEBF4FF);
  static const toggleButtonSelected = Color(0xff81E6D9);
  static const grey = Color(0xffCBD5E0);
  static const black = Color(0xff2D3748);
  static const greyCircle = Color(0xffF7FAFC);
  static const fontColor = Color(0xff718096);
  static const titleColor = Color(0xff718096);
  static const transparent = Colors.transparent;
}
