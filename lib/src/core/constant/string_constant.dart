class StringConstant {
  StringConstant._();

  static const undrawAgreement = 'asset/icon/undraw_agreement_aajr.svg';
  static const undrawProfileData = 'asset/icon/undraw_Profile_data_re_v81r.svg';
  static const undrawTask = 'asset/icon/undraw_task_31wc.svg';
  static const undrawPersonalFile = 'asset/icon/undraw_personal_file_222m.svg';
  static const undrawAboutMe = 'asset/icon/undraw_about_me_wa29.svg';
  static const undrawBusinessDeal = 'asset/icon/undraw_business_deal_cpi9.svg';
  static const undrawJobOffer = 'asset/icon/undraw_job_offers_kw5d.svg';
  static const firstArrow = 'asset/icon/first_arrow.svg';
  static const secondArrow = 'asset/icon/second_arrow.svg';
  static const undrawSwipeProfile =
      'asset/icon/undraw_swipe_profiles1_i6mr.svg';
}
