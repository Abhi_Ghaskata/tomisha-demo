import 'package:flutter/material.dart';

class AppTextTheme {
  AppTextTheme(this.context);

  final BuildContext context;

  TextTheme get _textTheme => Theme.of(context).textTheme;

  TextStyle? get hero => _textTheme.displayLarge?.copyWith(
        fontFamily: 'Lato',
        fontSize: 32,
        fontWeight: FontWeight.w500,
        height: 1.3,
      );

  TextStyle? get h1 => _textTheme.displayLarge?.copyWith(
        fontFamily: 'Lato',
        fontSize: 24,
        fontWeight: FontWeight.w500,
        height: 1.3,
      );

  TextStyle? get h2 => _textTheme.displayMedium?.copyWith(
        fontFamily: 'Lato',
        fontSize: 20,
        fontWeight: FontWeight.w600,
        height: 1.3,
      );

  TextStyle? get h3 => _textTheme.displaySmall?.copyWith(
        fontFamily: 'Lato',
        fontSize: 14,
        fontWeight: FontWeight.w500,
        height: 1.3,
      );

  TextStyle? get body => _textTheme.bodyMedium?.copyWith(
        fontFamily: 'Lato',
        fontSize: 16,
        fontWeight: FontWeight.w400,
        height: 1.5,
      );

  TextStyle? get bodySmall => _textTheme.bodyMedium?.copyWith(
        fontFamily: 'Lato',
        fontSize: 14,
        fontWeight: FontWeight.w400,
        height: 1.5,
      );

  TextStyle? get xSmallRegular => _textTheme.bodySmall?.copyWith(
        fontFamily: 'Lato',
        fontSize: 12,
        fontWeight: FontWeight.w400,
        height: 1.5,
      );

  TextStyle? get xSmallEmphasis => _textTheme.bodySmall?.copyWith(
        fontFamily: 'Lato',
        fontSize: 12,
        fontWeight: FontWeight.w600,
        height: 1.5,
      );

  TextStyle? get footnote => _textTheme.bodySmall?.copyWith(
        fontFamily: 'Lato',
        fontSize: 12,
        fontWeight: FontWeight.w400,
        height: 1.5,
      );

  TextStyle? get formPlaceholder => body?.copyWith(
        fontFamily: 'Lato',
        fontSize: 16,
        fontWeight: FontWeight.w400,
      );

  TextStyle? get miniLabel => _textTheme.bodySmall?.copyWith(
        fontFamily: 'Lato',
        fontSize: 10,
        fontWeight: FontWeight.w500,
        height: 1.5,
      );

  TextStyle? get button => _textTheme.labelLarge?.copyWith(
        fontFamily: 'Lato',
        fontSize: 16,
        fontWeight: FontWeight.w500,
      );
}
