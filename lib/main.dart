import 'package:flutter/material.dart';
import 'package:tomisha/src/feature/view/home_screen.dart';
import 'package:tomisha/src/widget/adaptive/adaptive_base_widget.dart';
import 'package:tomisha/src/widget/adaptive/adaptive_screen_type.dart';
import 'package:tomisha/src/widget/responsive/responsive_util_init.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AdaptiveBuilder(
      builder: (context, sizingInfo) {
        return ResponsiveUtilInit(
          designSize: sizingInfo.deviceType == AdaptiveScreenType.desktop
              ? const Size(1440, 1024)
              : sizingInfo.deviceType == AdaptiveScreenType.nativeMobile
                  ? const Size(390, 844)
                  : sizingInfo.deviceType == AdaptiveScreenType.webMobile
                      ? const Size(428, 844)
                      : const Size(752, 1280),
          builder: () => MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Flutter Demo',
            theme: ThemeData(
              primarySwatch: Colors.blue,
            ),
            routes: {
              '/': (context) => const HomeScreen(),
            },
            builder: (context, child) {
              // Any tap outside of a keyboard will dismiss the keyboard

              //causing issues on web where you have to double click
              return GestureDetector(
                onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
                // App will ignore users preference for dynamic text size

                child: MediaQuery(
                  data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
                  child: child!,
                ),
              );
            },
          ),
        );
      },
    );
  }
}
